package Bank;

import java.sql.Date;


public class Main {
    public static void main(String[] args) {
        Bank.Client client = new Bank.Client("Vasya", "Petrov", "Moscow", "264746546", Date.valueOf("1984-7-24"));
        Bank.Client client2 = new Bank.Client("Dima", "Sidorov", "St.Petersburg", "65843594", Date.valueOf("1993-3-29"));
        Bank.Bank bank = new Bank.Bank();
        bank.addClient(client);
        bank.addClient(client2);
        bank.addAccount(client);
        bank.printAccountsForClient(client);
        System.out.println();
        bank.putIntoAccount(1_000_000L, 300.0);
        bank.putIntoAccount(1_000_001L, 600.0);
        bank.moveFromOneAccountToAnother(1_000_001L, 1_000_000L, 100.0);
        bank.printAccounts();
        System.out.println();
        bank.printClients();
        System.out.println();
        bank.printTransactions();
        System.out.println();
        bank.printTransactionsForAccount(1_000_001L);
    }
}