package Bank;


public class Transaction {
    private Account source;
    private Operation operation;
    private double amount;

    public Transaction(Account source, Operation operation, double amount) {
        this.source = source;
        this.operation = operation;
        this.amount = amount;
    }


    public Account getSource() {
        return source;
    }

    public Operation getOperation() {
        return operation;
    }

    public double getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "Transaction: " +
                "account number " + source.getAccountNumber() +
                ", operation=" + operation +
                ", amount=" + amount + ";";
    }

}
