package Bank;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class Bank {
    private List<Client> clients = new ArrayList<>();
    private List<Account> accounts = new ArrayList<>();
    private List<Transaction> transactions = new ArrayList<>();

    //add
    public void addAccount(Client client) {
        Account account = new Account(Account.GLOBAL_ACCOUNT_NUMBER, client, 0.0);
        this.accounts.add(client.addAccount(account));
    }

    public boolean addClient(Client client) {
        if (findClient(client) != null) {
            System.out.println("Невозможно создать клиента. Он уже существует");
            return false;
        }
        this.clients.add(client);
        addAccount(client);
        return true;
    }

    //find
    public Client findClient(Client searchClient) {
        for (Client client : this.clients) {
            if (client.equals(searchClient)) return searchClient;
        }
        return null;
    }

    public Account findAccount(Long searchAccountsNumber) {
        for (Account account : accounts) {
            if (account.getAccountNumber().equals(searchAccountsNumber)) return account;
        }
        return null;
    }

    public List<Account> findAccountsByClient(Client searchClient) {
        return searchClient.getAccounts();
    }

    public List<Transaction> findTransactionsByAccount(Long searchAccountNumber) {
        List<Transaction> result = new ArrayList<>();
        for (Transaction transaction : transactions) {
            if (transaction.getSource().getAccountNumber().equals(searchAccountNumber)) {
                result.add(transaction);
            }
        }
        if (transactions == null) return null;
        return result;
    }

    //operation
    public boolean putIntoAccount(Long accountNumber, Double amount) {
        Account account = findAccount(accountNumber);
        if (account == null) {
            System.out.println("Невозможно выполнить операцию. Такого счета не существует");
            return false;
        } else if (amount <= 0) {
            System.out.println("Невозможно выполнить операцию. Пополнение на отрицательную сумму");
            return false;
        } else {
            account.increaseBalance(amount);
            this.transactions.add(new Transaction(account, Operation.PUT, amount));
            return true;
        }
    }

    public boolean withdrawFromAccount(Long accountNumber, Double amount) {
        Account account = findAccount(accountNumber);
        if (account == null) {
            System.out.println("Невозможно выполнить операцию. Такого счета не существует");
            return false;
        }
        if (amount <= 0) {
            System.out.println("Невозможно выполнить операцию. Снятие отрицательной суммы");
            return false;
        }
        if (account.getBalance() < amount) {
            System.out.println("Невозможно выполнить операцию. Недостаточно средств");
            return false;
        }
        account.decreaseBalance(amount);
        this.transactions.add(new Transaction(account, Operation.WITHDRAW, amount));
        return true;
    }

    public boolean moveFromOneAccountToAnother(Long sourceAccountNumber, Long destinationAccountNumber, Double amount) {
        if (withdrawFromAccount(sourceAccountNumber, amount) && findAccount(destinationAccountNumber) == null) {
            putIntoAccount(sourceAccountNumber, amount);
            return false;
        }
        putIntoAccount(destinationAccountNumber, amount);
        return true;
    }


    //print
    public void printClients() {
        for (Client client : this.clients) {
            System.out.println(client.toString());
        }
    }

    public void printAccounts() {
        Collections.sort(accounts);
        for (Account account : accounts) {
            System.out.println(account.toString());
        }
    }

    public void printAccountsForClient(Client client) {
        for (Account account : findAccountsByClient(client)) {
            System.out.println(account.toString());
        }
    }

    public void printTransactions() {
        for (Transaction transaction : transactions) {
            System.out.println(transaction.toString());
        }
    }

    void printTransactionsForAccount(Long accountNumber) {
        List<Transaction> result = findTransactionsByAccount(accountNumber);
        for (Transaction t : result) {
            System.out.println(t.toString());
        }
    }
}
